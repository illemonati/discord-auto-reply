import { DiscordUser } from "../utils/discord-utils";

export const updateDiscordUsers = (newState: DiscordUser[]) => {
    return {
        type: "UPDATE_DISCORD_USERS",
        payload: newState,
    };
};

export const addDiscordUser = (newUser: DiscordUser) => {
    return {
        type: "ADD_DISCORD_USER",
        payload: newUser,
    };
};

export default updateDiscordUsers;
