import { createMuiTheme, CssBaseline, ThemeProvider } from "@material-ui/core";
import React from "react";
import "./App.css";
import MainPageComponent from "./components/MainPage/MainPageComponent";
import "@openfonts/comic-neue_latin";

function App() {
    const theme = createMuiTheme({
        palette: {
            type: "dark",
            primary: {
                main: "#7289DA",
            },
            background: {
                default: "#202225",
                paper: "#2f3136",
            },
            action: {
                selected: "#393c42",
                hover: "rgba(79,84,92,0.16)",
            },
        },
        typography: {
            fontFamily: "Comic Neue",
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className="App">
                <MainPageComponent />
            </div>
        </ThemeProvider>
    );
}

export default App;
