import React, {useEffect, useState} from "react";
import {Container, Typography} from "@material-ui/core";
import './styles.css';
import LoginComponent from "../Login/LoginComponent";
import PostLoginComponent from "../PostLogin/PostLoginComponent";

const Discord = (window as any).Discord;

const MainPageComponent : React.FC = () => {
    const [client, setClient] = useState<Discord.Client | null>(null);

    useEffect(() => {
        (window as any).Discord = Discord;
    }, []);


    return (
        <div className="MainPageComponent">
            <Container>
                <Typography variant="h3">
                    Discord Auto Reply
                </Typography>
                <br />
                <br />
                <br />
                {
                    client ?
                        <PostLoginComponent client={client} /> :
                        <LoginComponent setClientCallback={setClient} />
                }
            </Container>
        </div>
    );
};


export default MainPageComponent;




