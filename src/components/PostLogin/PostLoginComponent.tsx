import React, {useEffect, useState} from "react";
import {Paper} from "@material-ui/core";
import SettingsComponent from "../Settings/SettingsComponent";
import {AutoReplySettings} from "../Settings/SettingsFormat";
import {ContactList} from "../../ContactList/ContactList";
import ContactListComponent from "../../ContactList/ContactListComponent";

const Discord = (window as any).Discord;


interface PostLoginComponentProps {
    client: Discord.Client
}


const PostLoginComponent: React.FC<PostLoginComponentProps> = (props: PostLoginComponentProps) => {
    const [settings, setSettings] = useState({
        replyMessage: {
            settingDisplayName: 'Reply Message',
            settingVal: 'Ok Boomer!'
        },
        timeDelay: {
            settingDisplayName: 'Time Delay (s)',
            settingVal: 5
        },
    } as AutoReplySettings);
    const client = props.client;
    const [contactList, setContactList] = useState([] as ContactList);
    useEffect(() => {
        client.user.friends.forEach((user) => {
            setContactList((contactList) => {
                const contact = {
                    user: user,
                    autoReplyOn: false
                };
                contactList.push(contact);
                return [...contactList];
            });
        });
        client.setMaxListeners(1);
        //eslint-disable-next-line
    }, [client]);

    useEffect(() => {
        const replyOnList = [] as string[];
        client.removeAllListeners();
        contactList.forEach(contact => {
            if (contact.autoReplyOn) {
                replyOnList.push(contact.user.id);
            }
        });

        client.on('message', (msg: Discord.Message) => {
            if (replyOnList.includes(msg.author.id)) {
                msg.channel.startTyping();
                setTimeout(() => {
                    msg.channel.stopTyping();
                    msg.reply(settings.replyMessage.settingVal).then();
                }, settings.timeDelay.settingVal * 1000);
            }
        });
    }, [contactList, client, settings]);

    return (
        <div className="PostLoginComponent">
            <Paper>
                <div className="paperInner">
                    <SettingsComponent client={client} settings={settings} updateSettingsCallback={setSettings}/>
                </div>
            </Paper>
            <br/>
            <br/>
            <Paper>
                <div className="paperInner">
                    <ContactListComponent contactList={contactList} setContactList={setContactList}/>
                </div>
            </Paper>
        </div>
    )
};


export default PostLoginComponent;
