import React, { ChangeEvent, useState } from "react";
import { Button, TextField, Typography } from "@material-ui/core";
import { loginWithToken } from "../../utils/discord-utils";
import InvalidLoginDialogComponent from "./InvalidLoginDialogComponent";
import { useDispatch } from "react-redux";
import { addDiscordUser } from "../../actions/discordUsers";

const Discord = (window as any).Discord;

interface DirectTokenInputComponentProps {
    setClientCallback: (client: Discord.Client) => any;
}

const DirectTokenInputComponent: React.FC<DirectTokenInputComponentProps> = (
    props: DirectTokenInputComponentProps
) => {
    const [invalidLoginDialogOpen, setInvalidLoginDialogOpen] = useState(false);
    const [token, setToken] = useState("");
    const dispatch = useDispatch();

    const handleLogin = async () => {
        try {
            const client = await loginWithToken(token);
            dispatch(
                addDiscordUser({
                    token: token,
                    username: client.user.tag,
                    avatarURL: client.user.avatarURL,
                })
            );
            props.setClientCallback(client);
        } catch (e) {
            setInvalidLoginDialogOpen(true);
        }
    };

    const handleTokenChange = (event: ChangeEvent<HTMLInputElement>) => {
        const newToken = event.currentTarget.value;
        setToken(newToken);
    };

    return (
        <div className="DirectTokenInputComponent">
            <InvalidLoginDialogComponent
                open={invalidLoginDialogOpen}
                setOpen={setInvalidLoginDialogOpen}
            />
            <Typography variant="h4">Please Enter Your Token</Typography>
            <br />
            <br />
            <TextField
                label="Token"
                color="primary"
                value={token}
                onChange={handleTokenChange}
                fullWidth
                margin="dense"
            />
            <br />
            <br />
            <br />
            <Button variant="outlined" color="primary" onClick={handleLogin}>
                Login
            </Button>
        </div>
    );
};

export default DirectTokenInputComponent;
