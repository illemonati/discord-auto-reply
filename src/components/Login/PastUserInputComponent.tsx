import React, { SyntheticEvent, useState } from "react";
import {
    Button,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Typography,
} from "@material-ui/core";
import { DiscordUser, loginWithToken } from "../../utils/discord-utils";
import InvalidLoginDialogComponent from "./InvalidLoginDialogComponent";
import { useSelector } from "react-redux";

const Discord = (window as any).Discord;

interface PastUserInputComponentProps {
    setClientCallback: (client: Discord.Client) => any;
}

const PastUserInputComponent: React.FC<PastUserInputComponentProps> = (
    props: PastUserInputComponentProps
) => {
    const [pastUsers] = useState(
        useSelector<any, DiscordUser[]>((state) => state.discordUsers)
    );
    const [invalidLoginDialogOpen, setInvalidLoginDialogOpen] = useState(false);
    const [token, setToken] = useState("");

    const handleLogin = async () => {
        try {
            const client = await loginWithToken(token);
            props.setClientCallback(client);
        } catch (e) {
            setInvalidLoginDialogOpen(true);
        }
    };

    const handleTokenChange = (newToken: string) => {
        setToken(newToken);
    };

    const handleImageError = (e: SyntheticEvent<HTMLImageElement>) => {
        e.currentTarget.src =
            "data:image/gif;base64,R0lGODlhAQABAID/ AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
    };

    return (
        <div className="PastUserInputComponent">
            <InvalidLoginDialogComponent
                open={invalidLoginDialogOpen}
                setOpen={setInvalidLoginDialogOpen}
            />
            <Typography variant="h4">
                Please Select From This List of Past Logins
            </Typography>
            <br />
            <br />
            <List className="pastUsersList">
                {pastUsers.map((pastUser, i) => {
                    return (
                        <ListItem
                            button
                            key={i}
                            onClick={() => handleTokenChange(pastUser.token)}
                            selected={token === pastUser.token}
                        >
                            <ListItemIcon>
                                <img
                                    alt=""
                                    onError={handleImageError}
                                    src={pastUser.avatarURL}
                                    className="listAvatar"
                                />
                            </ListItemIcon>
                            <ListItemText
                                primary={pastUser.username}
                                secondary={pastUser.token}
                                className="listUserTag"
                            />
                        </ListItem>
                    );
                })}
            </List>
            <br />
            <br />
            <br />
            {token && (
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={handleLogin}
                >
                    Login
                </Button>
            )}
        </div>
    );
};

export default PastUserInputComponent;
