import React, { ChangeEvent, useState } from "react";
import { Paper, Tab, Tabs } from "@material-ui/core";
import DirectTokenInputComponent from "./DirectTokenInputComponent";
import EmailPasswordInputComponent from "./EmailPasswordInputComponent";
import PastUserInputComponent from "./PastUserInputComponent";
import SwipeableViews from "react-swipeable-views";

const Discord = (window as any).Discord;

interface LoginComponentProps {
    setClientCallback: (client: Discord.Client) => any;
}

const LoginComponent: React.FC<LoginComponentProps> = (
    props: LoginComponentProps
) => {
    const [tabValue, setTabValue] = useState(0);

    const handleTabChange = (e: ChangeEvent<{}>, newVal: number) => {
        setTabValue(newVal);
    };

    const handleSwipe = (newVal: number) => {
        setTabValue(newVal);
    };

    return (
        <div className="LoginComponent">
            <Paper>
                <Tabs
                    value={tabValue}
                    onChange={handleTabChange}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label="Email & Password" />
                    <Tab label="Token" />
                    <Tab label="Past User" />
                </Tabs>
            </Paper>

            <br />
            <br />
            <SwipeableViews index={tabValue} onChangeIndex={handleSwipe}>
                <Paper>
                    <div className="paperInner">
                        <EmailPasswordInputComponent
                            setClientCallback={props.setClientCallback}
                        />
                    </div>
                </Paper>

                <Paper>
                    <div className="paperInner">
                        <DirectTokenInputComponent
                            setClientCallback={props.setClientCallback}
                        />
                    </div>
                </Paper>
                <Paper>
                    <div className="paperInner">
                        <PastUserInputComponent
                            setClientCallback={props.setClientCallback}
                        />
                    </div>
                </Paper>
            </SwipeableViews>
        </div>
    );
};

export default LoginComponent;
