import React, { ChangeEvent, useState } from "react";
import { Button, TextField, Typography } from "@material-ui/core";
import {
    EmailPassword,
    getTokenFromEmailPassword,
    loginWithToken,
} from "../../utils/discord-utils";
import InvalidLoginDialogComponent from "./InvalidLoginDialogComponent";
import { useDispatch } from "react-redux";
import { addDiscordUser } from "../../actions/discordUsers";

const Discord = (window as any).Discord;

interface EmailPasswordInputComponentProps {
    setClientCallback: (client: Discord.Client) => any;
}

const EmailPasswordInputComponent: React.FC<EmailPasswordInputComponentProps> = (
    props: EmailPasswordInputComponentProps
) => {
    const dispatch = useDispatch();
    const [config, setConfig] = useState({
        Email: "",
        Password: "",
    } as EmailPassword);

    const [invalidLoginDialogOpen, setInvalidLoginDialogOpen] = useState(false);

    const handleConfigChange = (fieldName: string) => (
        e: ChangeEvent<HTMLInputElement>
    ) => {
        const newVal = e.currentTarget.value;
        setConfig((config) => {
            config[fieldName] = newVal;
            return { ...config };
        });
    };

    const handleLogin = async () => {
        try {
            const token = await getTokenFromEmailPassword(config);
            const client = await loginWithToken(token);
            dispatch(
                addDiscordUser({
                    token: token,
                    username: client.user.tag,
                    avatarURL: client.user.avatarURL,
                })
            );
            props.setClientCallback(client);
        } catch (e) {
            setInvalidLoginDialogOpen(true);
        }
    };

    return (
        <div className="EmailPasswordInputComponent">
            <InvalidLoginDialogComponent
                open={invalidLoginDialogOpen}
                setOpen={setInvalidLoginDialogOpen}
            />
            <Typography variant="h4">Please Enter Your Login Info</Typography>
            <Typography variant="subtitle1">
                Note: Email & Password login will not work if you haven't logged
                in to official discord from this IP previously
            </Typography>
            <Typography variant="subtitle1">
                Note2: Apparently discord blocks all access to /auth/login from
                outside their services and localhost. Just use the token login
                instead. (AKA. CORS Problem, If you know what that means u know
                how to deal with it)
            </Typography>
            <br />
            <br />
            {Object.keys(config).map((fieldName, i) => {
                const fieldType =
                    fieldName === "Password" ? "password" : "text";
                return (
                    <React.Fragment key={i}>
                        <TextField
                            label={fieldName}
                            color="primary"
                            type={fieldType}
                            onChange={handleConfigChange(fieldName)}
                            fullWidth
                            margin="dense"
                        />
                        <br />
                        <br />
                    </React.Fragment>
                );
            })}
            <br />
            <Button variant="outlined" color="primary" onClick={handleLogin}>
                Login
            </Button>
        </div>
    );
};

export default EmailPasswordInputComponent;
