import React from "react";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography} from "@material-ui/core";

interface InvalidLoginDialogComponentProps {
    open: boolean;
    setOpen: (open: boolean) => any;
}

const InvalidLoginDialogComponent: React.FC<InvalidLoginDialogComponentProps> = props => {
    const invalidLoginDialogOpen = props.open;
    const setInvalidLoginDialogOpen = props.setOpen;
    return (
        <Dialog open={invalidLoginDialogOpen}
                onClose={() => setInvalidLoginDialogOpen(false)}
                fullWidth
        >
            <DialogTitle>
                Invalid Login
            </DialogTitle>
            <DialogContent>
                <Typography variant="subtitle1">Please try again. I swear, these younger generation are getting more and more careless.</Typography>
                <Typography variant="subtitle1">Back in my Days ........ talks for 10 hours </Typography>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => setInvalidLoginDialogOpen(false)}>Ok Boomer!</Button>
            </DialogActions>
        </Dialog>
    );
};

export default InvalidLoginDialogComponent;

