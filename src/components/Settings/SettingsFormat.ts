


export interface AutoReplySetting<T> {
    settingDisplayName: string;
    settingVal: T;
}

export interface AutoReplySettings {
    replyMessage: AutoReplySetting<string>;
    timeDelay: AutoReplySetting<number>;
    [key: string]: AutoReplySetting<any>;
}


