import React, {useEffect, useState} from "react";
import {Grid} from "@material-ui/core";
import {AutoReplySetting, AutoReplySettings} from "./SettingsFormat";
import IconButton from '@material-ui/core/IconButton';
import CreateIcon from '@material-ui/icons/Create';
import './styles.css';
import SettingsChangeComponent from "./SettingsChangeComponent";

const Discord = (window as any).Discord;


interface SettingsComponentProps {
    client: Discord.Client
    settings: AutoReplySettings
    updateSettingsCallback: (arg: any) => any
}

const SettingsComponent: React.FC<SettingsComponentProps> = (props: SettingsComponentProps) => {
    const settings = props.settings;
    const [dialogsOpen, setDialogsOpen] = useState({} as { [key: string]: boolean });

    useEffect(() => {
        Object.keys(settings).forEach((setting) => {
            setDialogsOpen(dialogs => {
                dialogs[setting] = false;
                return dialogs;
            });
        });
        //eslint-disable-next-line
    }, []);

    const handleSettingChange = (setting: string, newVal: AutoReplySetting<any>) => {
        const newSettings = {...props.settings};
        if (typeof settings[setting].settingVal !== 'number') {
            newSettings[setting] = newVal;
        } else {
            newVal.settingVal = parseInt(newVal.settingVal);
            newSettings[setting] = newVal;
        }

        props.updateSettingsCallback(newSettings)
    };


    return (
        <div className="SettingsComponent">
            <Grid container spacing={10} className="SettingsGrid">

                <Grid item xs={6} className="vertCenterAlign">
                    User Tag:
                </Grid>
                <Grid item xs={6} className="vertCenterAlign">
                    {props.client.user.tag}
                </Grid>
                <Grid item xs={6} className="vertCenterAlign">
                    User Avatar:
                </Grid>
                <Grid item xs={6} className="vertCenterAlign ">
                    <img src={props.client.user.avatarURL}
                         alt=''
                         className="userAvatar"
                    />
                </Grid>
                {Object.keys(props.settings).map((setting, i) => {
                    const handleDialogOpenChange = (newVal: boolean) => {
                        setDialogsOpen(diaopen => {
                            diaopen[setting] = newVal;
                            return {...diaopen};
                        })
                    };
                    return (
                        <React.Fragment key={i}>
                            <Grid item xs={6} className="vertCenterAlign">
                                {props.settings[setting].settingDisplayName}
                            </Grid>
                            <Grid item xs={6} className="vertCenterAlign ">
                                <pre className="settingDisplay">{props.settings[setting].settingVal}</pre>
                                <IconButton onClick={() => handleDialogOpenChange(true)}>
                                    <CreateIcon/>
                                </IconButton>
                                <SettingsChangeComponent setting={settings[setting]}
                                                         settingCallBack={(newSettingVal) => {
                                                             handleSettingChange(setting, newSettingVal)
                                                         }}
                                                         open={dialogsOpen[setting]}
                                                         setOpenCallback={handleDialogOpenChange}/>
                            </Grid>
                        </React.Fragment>
                    )
                })}
            </Grid>
        </div>
    );
};

export default SettingsComponent;

