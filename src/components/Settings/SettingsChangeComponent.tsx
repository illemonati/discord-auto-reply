import React, {ChangeEvent} from "react";
import {
    Button,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, TextField,
} from "@material-ui/core";
import {AutoReplySetting} from "./SettingsFormat";

interface SettingsChangeComponentProps {
    setting: AutoReplySetting<string | number>;
    settingCallBack: (newSetting: any) => any;
    open?: boolean;
    setOpenCallback: (open: boolean) => any;
}

const SettingsChangeComponent: React.FC<SettingsChangeComponentProps> = (props: SettingsChangeComponentProps) => {

    const open = props.open || false;
    const setOpenCallback = props.setOpenCallback;

    const handleSettingChange = (e: ChangeEvent<HTMLInputElement>) => {
        const newVal = e.currentTarget.value;
        const newSetting = {...props.setting};
        newSetting.settingVal = newVal;
        props.settingCallBack(newSetting);
    };


    return (
        <Dialog open={open}
                onClose={() => setOpenCallback(false)}
                fullWidth
        >
            <Container>
                <DialogTitle>
                    Change {props.setting.settingDisplayName}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>Enter a new value
                        for {props.setting.settingDisplayName} zoomer!</DialogContentText>
                    <TextField autoFocus
                               className="settingDisplay"
                               margin="dense"
                               label={props.setting.settingDisplayName}
                               value={props.setting.settingVal}
                               onChange={handleSettingChange}
                               multiline={typeof props.setting.settingVal === "string"}
                               type={typeof props.setting.settingVal}
                               fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenCallback(false)}>Done Boomer!</Button>
                </DialogActions>
            </Container>
        </Dialog>
    )
};

export default SettingsChangeComponent;


