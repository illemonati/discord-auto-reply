import axios from "axios";

const Discord = (window as any).Discord;

export const loginWithToken = async (
    botToken: string
): Promise<Discord.Client> => {
    const client = new Discord.Client({
        ws: {
            compress: false,
        },
    });
    try {
        await client.login(botToken);
        // console.log(client);
        return client;
    } catch (e) {
        throw e;
    }
};

export interface EmailPassword {
    Email: string;
    Password: string;

    [key: string]: string;
}

export interface DiscordUser {
    token: string;
    username: string;
    avatarURL: string;
}

export const getTokenFromEmailPassword = async (config: EmailPassword) => {
    const payload = {
        email: config.Email,
        password: config.Password,
    };

    try {
        const resp = await axios.post(
            "https://discordapp.com/api/auth/login",
            payload
        );
        console.log(resp.data["token"]);
        return resp.data["token"];
    } catch (e) {
        throw e;
    }
};
