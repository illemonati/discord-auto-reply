import React, {SyntheticEvent} from "react";
import {ContactList} from "./ContactList";
import {List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import './styles.css';

interface ContactListComponentProps {
    contactList: ContactList
    setContactList: (contactList: any) => any
}

const ContactListComponent: React.FC<ContactListComponentProps> = (props: ContactListComponentProps) => {
    const contactList = props.contactList;
    const setContactList = props.setContactList;

    const handleListItemToggle = (index: number) => () => {
        setContactList((list: ContactList) => {
            list[index].autoReplyOn = !list[index].autoReplyOn;
            return list.slice();
        });
    };

    const handleImageError = (e: SyntheticEvent<HTMLImageElement>) => {
        e.currentTarget.src = 'data:image/gif;base64,R0lGODlhAQABAID/ AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
    };

    return (
        <div className="ContactListComponent">
            <List>
                {contactList.map((contact, i) => {
                    const totalName = contact.user.tag;
                    return (
                        <ListItem button
                                  key={i}
                                  onClick={handleListItemToggle(i)}
                                  selected={contactList[i].autoReplyOn}
                        >
                            <ListItemIcon>
                                <img alt=''
                                     onError={handleImageError}
                                     src={contact.user.avatarURL}
                                     className="listAvatar"
                                />
                            </ListItemIcon>
                            <ListItemText primary={totalName}
                                          className="listUserTag"
                            />
                        </ListItem>
                    );
                })}
            </List>
        </div>
    )
};

export default ContactListComponent;


