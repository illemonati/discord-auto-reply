const Discord = (window as any).Discord;


export type ContactList = Contact[];

export interface Contact {
    user: Discord.User
    autoReplyOn: boolean
}


