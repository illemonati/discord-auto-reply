import { combineReducers } from "redux";
import discordUsersReducer from "./discordUsers";

const rootReducer = combineReducers({
    discordUsers: discordUsersReducer,
});

export default rootReducer;
