import { DiscordUser } from "../utils/discord-utils";

const discordUsersReducer = (
    state: DiscordUser[] = [],
    action: any
): DiscordUser[] => {
    switch (action.type) {
        case "UPDATE_DISCORD_USERS":
            return action.payload;
        case "ADD_DISCORD_USER":
            const newState = state.slice();
            newState.push(action.payload as DiscordUser);
            return newState;
        default:
            return state;
    }
};

export default discordUsersReducer;
